# NATFLARE
####   NatFlare is tool for bypass NAT-Connection over SSH-Reverse Proxy
 *This project currently is in development*

[![CodeFactor](https://www.codefactor.io/repository/github/nekofade/natflare/badge/master)](https://www.codefactor.io/repository/github/nekofade/natflare/overview/master) [![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-green.svg)](https://github.com/nekofade/natflare/graphs/commit-activity) [![made-with-bash](https://img.shields.io/badge/Made%20with-Bash-1f425f.svg)](https://www.gnu.org/software/bash/) [![GPLv3 license](https://img.shields.io/badge/License-GPLv3-blue.svg)](http://perso.crans.org/besson/LICENSE.html)

